package com.example.tictac

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(),View.OnClickListener {
    private lateinit var button1:Button
    private lateinit var button2:Button
    private lateinit var button3:Button
    private lateinit var button4:Button
    private lateinit var button5:Button
    private lateinit var button6:Button
    private lateinit var button7:Button
    private lateinit var button8:Button
    private lateinit var button9:Button
    private var activeplayer = 1
    private lateinit var resetbtn:Button
    private var firstplayer = ArrayList<Int>()
    private var secondplayer = ArrayList<Int>()
    private var score1 = 0
    private var score2 = 0
    private lateinit var scorefirst: TextView
    private lateinit var scoresecond: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        firstplayer
        resetbtn = findViewById(R.id.resetbtn)
        resetbtn.setOnClickListener{
            reset()
            score1 = 0
            score2 = 0
            scorefirst.text = "SCORE P1: 0"
            scoresecond.text = "SCORE P2: 0"
        }
        scorefirst = findViewById(R.id.scorefirst)
        scoresecond = findViewById(R.id.scoresecond)
    }
    private fun init(){
        button1 = findViewById(R.id.button1)
        button2 = findViewById(R.id.button2)
        button3 = findViewById(R.id.button3)
        button4 = findViewById(R.id.button4)
        button5 = findViewById(R.id.button5)
        button6 = findViewById(R.id.button6)
        button7 = findViewById(R.id.button7)
        button8 = findViewById(R.id.button8)
        button9 = findViewById(R.id.button9)

        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
    }

    override fun onClick(clickedView: View?) {
        if (clickedView is Button){
            var buttonnumber = 0
            when(clickedView.id){
                R.id.button1 -> buttonnumber = 1
                R.id.button2 -> buttonnumber = 2
                R.id.button3 -> buttonnumber = 3
                R.id.button4 -> buttonnumber = 4
                R.id.button5 -> buttonnumber = 5
                R.id.button6 -> buttonnumber = 6
                R.id.button7 -> buttonnumber = 7
                R.id.button8 -> buttonnumber = 8
                R.id.button9 -> buttonnumber = 9
            }
            if(buttonnumber!=0){
                playgame(clickedView,buttonnumber)
            }
        }
    }
    private fun playgame(clickedView: Button, buttonnumber:Int){
        if(activeplayer == 1){
            clickedView.text = "X"
            activeplayer = 2
            firstplayer.add(buttonnumber)
        }else{
            clickedView.text = "0"
            activeplayer = 1
            secondplayer.add(buttonnumber)
        }
        clickedView.isEnabled = false
        check()
    }

    private fun check(){
        var winnerplayer = 0
        if(firstplayer.contains(1) && firstplayer.contains(2) && firstplayer.contains(3)){
            winnerplayer = 1
        }
        if(secondplayer.contains(1) && secondplayer.contains(2) && secondplayer.contains(3)){
            winnerplayer = 2
        }
        if(firstplayer.contains(4) && firstplayer.contains(5) && firstplayer.contains(6)){
            winnerplayer = 1
        }
        if(secondplayer.contains(4) && secondplayer.contains(5) && secondplayer.contains(6)){
            winnerplayer = 2
        }
        if(firstplayer.contains(7) && firstplayer.contains(8) && firstplayer.contains(9)){
            winnerplayer = 1
        }
        if(secondplayer.contains(7) && secondplayer.contains(8) && secondplayer.contains(9)){
            winnerplayer = 2
        }
        if(firstplayer.contains(1) && firstplayer.contains(4) && firstplayer.contains(7)){
            winnerplayer = 1
        }
        if(secondplayer.contains(1) && secondplayer.contains(4) && secondplayer.contains(7)){
            winnerplayer = 2
        }
        if(firstplayer.contains(2) && firstplayer.contains(5) && firstplayer.contains(8)){
            winnerplayer = 1
        }
        if(secondplayer.contains(2) && secondplayer.contains(5) && secondplayer.contains(8)){
            winnerplayer = 2
        }
        if(firstplayer.contains(3) && firstplayer.contains(6) && firstplayer.contains(9)){
            winnerplayer = 1
        }
        if(secondplayer.contains(3) && secondplayer.contains(6) && secondplayer.contains(9)){
            winnerplayer = 2
        }
        if(firstplayer.contains(1) && firstplayer.contains(5) && firstplayer.contains(9)){
            winnerplayer = 1
        }
        if(secondplayer.contains(1) && secondplayer.contains(5) && secondplayer.contains(9)){
            winnerplayer = 2
        }
        if(firstplayer.contains(3) && firstplayer.contains(5) && firstplayer.contains(7)){
            winnerplayer = 1
        }
        if(secondplayer.contains(3) && secondplayer.contains(5) && secondplayer.contains(7)){
            winnerplayer = 2
        }
        if(winnerplayer == 1){
            Toast.makeText(this, "First player won", Toast.LENGTH_SHORT).show()
            score1 ++
            reset()
            scorefirst.text = "SCORE P1: $score1"
        }else if (winnerplayer == 2){
            Toast.makeText(this, "Second player won", Toast.LENGTH_SHORT).show()
            score2 ++
            reset()
            scoresecond.text = "SCORE P2: $score2"

        }else if(firstplayer.size+secondplayer.size==9){
            reset()
            Toast.makeText(this, "DRAW", Toast.LENGTH_SHORT).show()

        }


    }
    private fun reset(){
        button1.text = ""
        button2.text = ""
        button3.text = ""
        button4.text = ""
        button5.text = ""
        button6.text = ""
        button7.text = ""
        button8.text = ""
        button9.text = ""

        button1.isEnabled = true
        button2.isEnabled = true
        button3.isEnabled = true
        button4.isEnabled = true
        button5.isEnabled = true
        button6.isEnabled = true
        button7.isEnabled = true
        button8.isEnabled = true
        button9.isEnabled = true
        activeplayer = 1
        firstplayer.clear()
        secondplayer.clear()
    }
}